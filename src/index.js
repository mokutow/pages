angular
    .module('app', ['mb-scrollbar', 'ngRoute', 'datePicker'])
    .config(function ($routeProvider) {
        $routeProvider.
            when('/login', {
                template: '<auth-login></auth-login>'
            }).
            when('/signup', {
                template: '<auth-signup></auth-signup>'
            }).
            when('/app', {
                template: '<application></application>'
            }).
            when('/forgot', {
                template: '<auth-forgot></auth-forgot>'
            }).
            otherwise({
                redirectTo: '/login'
            });
    });