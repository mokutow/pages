angular
  .module('app')
  .controller('AsideMenu', function (Todo, ActionsMenu) {
    this.todo = Todo
    this.actions = ActionsMenu;
    this.selectProject = Todo.selectProject
    this.selectPriority = Todo.selectPriority
  })

angular
  .module('app')
  .directive('asideMenu', function () {
    return {
      restrict: 'E',
      templateUrl: 'app/asidemenu/AsideMenu.html',
      controller: 'AsideMenu',
      controllerAs: 'menu'
    }
  })
