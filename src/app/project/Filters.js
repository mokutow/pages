angular
  .module('app')
  .filter('priorityFilter', function (Todo) {
    return function (tasks) {
      tasks.forEach(function (t) {
        if (Todo.selectedPriority) {
          t.inFilter = (t.priority == Todo.selectedPriority)
        } else {
          t.inFilter = true;
        }
      })
      return tasks;
    }
  })