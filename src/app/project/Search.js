angular
    .module('app')
    .directive('searchBox', function () {
        return {
            restrict: 'E',
            templateUrl: 'app/project/Search.html',
            controller: 'SearchBox',
            controllerAs: 'search'
        }
    })

angular
    .module('app')
    .controller('SearchBox', function (Todo, ActionsMenu) {
        var self = this;
        this.todo = Todo;
        this.members = ActionsMenu.Project.members;
        this.membersView = ActionsMenu.Project.membersView;
    })

