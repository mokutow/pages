angular
    .module('app')
    .factory('UsersService', function () {
        var service = {};
        service.getUser = function () {
            return {
                name: 'Nelson King',
                email: 'nelson@gmail.com',
                password: 'password',
                img: 'assets/img/users/7.jpg'
            }
        }
        return service;
    })