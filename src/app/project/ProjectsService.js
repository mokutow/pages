var user = {
  name: 'Nelson King',
  email: 'nelson@gmail.com',
  password: 'password',
  img: 'assets/img/users/7.jpg'

}

var members1 = [{
  'id': 1,
  'name': 'Alison',
  'img': 'assets/img/users/1.jpg'
}, {
    'id': 2,
    'name': 'Andrew',
    'img': 'assets/img/users/2.jpg'
  }, {
    'id': 3,
    'name': 'Jenifer',
    'img': 'assets/img/users/8.jpg'
  }]

var members2 = [{
  'id': 4,
  'name': 'Irish',
  'img': 'assets/img/users/4.jpg'
}, {
    'id': 5,
    'name': 'Gringo',
    'img': 'assets/img/users/5.jpg'
  }, {
    'id': 6,
    'name': 'Jeremy',
    'img': 'assets/img/users/6.jpg'
  }]

var members3 = [{
  'id': 7,
  'name': 'Reddison',
  'img': 'assets/img/users/7.jpg'
}]
var day2 = {
  date: '2015.12.12',
  tasks: [{
    'date': '2015.12.12',
    'title': 'Create a company',
    'priority': 'yellow',
    'description': 'Very long and beautiful description. Very long and beautiful description. Very long and beautiful description. Very long and beautiful description. ',
    'members': members1,
    'creator': user,
    'tags': ['HTML'],
  }, {
      'date': '2015.12.12',
      'title': 'Buy capuccino',
      'description': 'Very long and beautiful description. Very long and beautiful description. Very long and beautiful description. Very long and beautiful description. ',
      'priority': 'red',
      'creator': user,
      'members': members3,
      'tags': ['HTML'],
    }]
}

var day1 = {
  date: '2015.12.20',
  tasks: [{
    'date': '2015.12.20',
    'title': 'Solve puzzle',
    'description': 'Very long and beautiful description. Very long and beautiful description. Very long and beautiful description. Very long and beautiful description. ',
    'priority': 'orange',
    'creator': user,
    'tags': ['HTML'],
    'members': members2
  }]
}

var day3 = {
  date: '2015.12.30',
  tasks: [{
    'date': '2015.12.30',
    'title': 'Solve something',
    'description': 'Very long and beautiful description. Very long and beautiful description. Very long and beautiful description. Very long and beautiful description. ',
    'priority': 'yellow',
    'members': members3,
    'creator': user,
    'tags': ['TOKYO']
  }]
}

var tasksFor1 = [day1, day2];

var allMembers = [].concat(members1).concat(members2).concat(members3);

var projects = [
  {
    id: 1,
    title: 'Private',
    members: members1,
    tasks: tasksFor1,
    count: 3
  }, {
    id: 2,
    title: 'Family',
    icon: 'ion-android-person',
    members: members2,
    tasks: [day3],
    count: 1
  },
]

var tags = {
  '1': [{
    title: 'HTML',
    color: '#4BB14F'
  }, {
      title: 'WEB',
      color: '#00ABF4'
    }, {
      title: 'Design',
      color: '#9D22B2'
    }, {
      title: 'Server',
      color: '#9F9F9F'
    }],
  '2': [
    {
      title: 'Unicorn',
      color: '#4BB14F'
    },
    {
      title: 'TOKYO',
      color: 'red'
    }
  ]
}

var priorities = [{ title: 'Red' }, { title: 'Orange' }, { title: 'Yellow' }, { title: 'White' }]



angular
  .module('app')
  .service('ProjectsService', function () {
    var getTags = function () {
      return tags
    }
    this.getProjects = function () {
      return projects.map(function (p) {
        p.tags = getTags()[p.id];
        return p;
      })
    }
    this.getPriorities = function () {
      return priorities
    }
    this.allMembers = function () {
      return allMembers;
    }
  })
