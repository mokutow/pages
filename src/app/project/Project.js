angular
  .module('app')
  .directive('project', function () {
    return {
      restrict: 'E',
      templateUrl: 'app/project/Project.html',
      controller: 'Tasks',
      controllerAs: 'tasks'
    }
  })

angular
  .module('app')
  .controller('Tasks', function (Todo, ActionsMenu) {
    var self = this;
    this.todo = Todo
    this.taskView = ActionsMenu.Task.view;
    this.taskCreate = ActionsMenu.Task.create;
    this.removeTask = function (task, day) {
      day.tasks.splice(day.tasks.indexOf(task), 1);
      Todo.selectedProject.count--;
      if (day.tasks.length == 0) {
        Todo.selectedProject.tasks.splice(Todo.selectedProject.tasks.indexOf(day), 1);
      }
    }
  })

