angular
  .module('app')
  .directive('application', function () {
    return {
      restrict: 'E',
      templateUrl: 'app/App.html'
    }
  })
  .controller('Application', function (ActionsMenu) {
    this.close = ActionsMenu.close;
  })

angular
  .module('app')
  .factory('Todo', function (ProjectsService, UsersService) {
    var todo = {};
    todo.user = UsersService.getUser();
    todo.projects = ProjectsService.getProjects()
    todo.priorities = ProjectsService.getPriorities()
    todo.allMembers = ProjectsService.allMembers()
    todo.selectedProject = false
    todo.selectedPriority = false
    todo.selectProject = function (project) {
      todo.projects.map(function (p) { p.selected = false })
      todo.selectedProject = project
      project.isNew = false;
      project.selected = true
    }
    todo.selectPriority = function (priority) {
      if (priority.selected) {
        priority.selected = false
        todo.selectedPriority = false
      } else {
        todo.priorities.map(function (p) { p.selected = false })
        todo.selectedPriority = priority.title.toLowerCase()
        priority.selected = true
      }
    }

    todo.selectTag = function (tag) {
      if (tag.selected) {
        tag.selected = false
        todo.selectedTag = false
      } else {
        todo.selectedProject.tags.map(function (p) { p.selected = false })
        todo.selectedTag = tag.title;
        tag.selected = true
      }
    }

    return todo
  })
