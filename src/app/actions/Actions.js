angular
  .module('app')
  .factory('ActionsMenu', function (Todo, $timeout, $document) {
    var actions = {}
    actions.opened = false
    actions.action = '';
    // Tasks
    // task:view
    actions.Task = {};
    actions.Task.view = function (task, day) {
      task.isNew = false;
      Todo.selectedTask = task;
      Todo.selectedDay = day;
      actions.open('task:view');
    }
    actions.Task.create = function () {
      actions.open('task:create');
    }
    actions.Task.edit = function () {
      actions.open('task:edit');
    }
    // Profile
    actions.Profile = {};
    actions.Profile.view = function () {
      actions.open('profile:view');
    }
    // Projects
    actions.Project = {};
    actions.Project.create = function () {
      actions.open('project:create');
    }
    actions.Project.edit = function () {
      actions.open('project:edit');
    }
    actions.Project.tags = function () {
      actions.open('project:tags');
    }
    actions.Project.members = function () {
      actions.open('project:members');
    }
    actions.Project.membersView = function () {
      actions.open('project:members:view');
    }
    // Common
    actions.open = function (action) {
      if (actions.action == action) return;
      if (actions.action) {
        actions.close();
        $timeout(function () {
          actions.open(action);
        }, 300);
      } else {
        actions.action = action;
        actions.opened = true;
      }
    }
    actions.close = function () {
      actions.action = false;
      actions.opened = false
    }
    return actions
  })

angular
  .module('app')
  .controller('ActionsCtrl', function (ActionsMenu, $document) {
    this.actions = ActionsMenu
    this.close = ActionsMenu.close
  })

angular
  .module('app')
  .directive('clickAnywhere', function ($document) {
    return {
      restrict: 'A',
      link: function postLink(scope, element, attrs) {
        var onClick = function (event) {
          var isChild = $(element).has(event.target).length > 0;
          var isSelf = element[0] == event.target;
          var isInside = isChild || isSelf;
          if (!isInside) {
            scope.$apply(attrs.clickAnywhereButHere)
          }
        }
        scope.$watch(attrs.isActive, function (newValue, oldValue) {
          if (newValue !== oldValue && newValue == true) {
            $document.bind('click', onClick);
          }
          else if (newValue !== oldValue && newValue == false) {
            $document.unbind('click', onClick);
          }
        });
      }
    }
  })

angular
  .module('app')
  .directive('actions', function () {
    return {
      restrict: 'E',
      templateUrl: 'app/actions/Actions.html',
      controller: 'ActionsCtrl',
      controllerAs: 'vm'
    }
  })