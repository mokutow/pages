angular
    .module('app')
    .directive('profileView', function () {
        return {
            restrict: 'E',
            templateUrl: 'app/actions/parts/profile/View.html',
            controller: "ProfileView",
            controllerAs: "profile"
        }
    }).controller('ProfileView', function (ActionsMenu, Todo) {
        var self = this;
        this.name = Todo.user.name;
        this.password = Todo.user.password;
        this.email = Todo.user.email;
        this.img = Todo.user.img;
        this.save = function () {
            Todo.user.name = self.name;
            Todo.user.email = self.email;
            Todo.user.img = self.img;
            ActionsMenu.close();
        }
        this.deleteAvatar = function () {
            self.img = "assets/img/users/user.png"
        }
    })