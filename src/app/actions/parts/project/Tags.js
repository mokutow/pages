angular
    .module('app')
    .directive('projectTags', function () {
        return {
            restrict: 'E',
            templateUrl: 'app/actions/parts/project/Tags.html',
            controller: "ProjectTags",
            controllerAs: "tags"
        }
    }).controller('ProjectTags', function (ActionsMenu, Todo) {
        var self = this;
        this.tags = angular.copy(Todo.selectedProject.tags);
        this.save = function () {
            Todo.selectedProject.tags = self.tags;
            ActionsMenu.close();
        }
        this.deleteTag = function (tag) {
            self.tags.splice(self.tags.indexOf(tag), 1);
        }
        this.createTag = function() {
            self.tags.push({
                title: "",
                color: ""
            })   
        }
    })