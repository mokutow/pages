angular
    .module('app')
    .directive('projectMembersView', function () {
        return {
            restrict: 'E',
            templateUrl: 'app/actions/parts/project/ViewMembers.html',
            controller: "ProjectMembersView",
            controllerAs: "members"
        }
    }).controller('ProjectMembersView', function (ActionsMenu, Todo) {
        var self = this;
        this.members = angular.copy(Todo.selectedProject.members);

    })