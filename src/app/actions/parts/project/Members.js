angular
    .module('app')
    .directive('projectMembers', function () {
        return {
            restrict: 'E',
            templateUrl: 'app/actions/parts/project/Members.html',
            controller: "ProjectMembers",
            controllerAs: "members"
        }
    }).controller('ProjectMembers', function (ActionsMenu, Todo) {
        var self = this;
        this.members = angular.copy(Todo.allMembers);
        this.members.forEach(function (m) {
            if (isInProject(m)) {
                m.isSelected = true;
            }
        });
        function isInProject(member) {
            var members = Todo.selectedProject.members;
            for (i in Todo.selectedProject.members) {
                if (member.id == members[i].id) return true;
            }
            return false;
        }
        this.toggle = function (member) {
            member.isSelected = !member.isSelected;
        }
        this.save = function () {
            var members = self.members.filter(function (m) {
                return m.isSelected;
            })
            Todo.selectedProject.members = members;
            ActionsMenu.close();
        }
    })