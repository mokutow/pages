angular
    .module('app')
    .directive('projectCreate', function () {
        return {
            restrict: 'E',
            templateUrl: 'app/actions/parts/project/Create.html',
            controller: "ProjectCreate",
            controllerAs: "project"
        }
    }).controller('ProjectCreate', function (ActionsMenu, Todo) {
        var self = this;
        this.members = angular.copy(Todo.allMembers);
        this.members.forEach(function(m) { m.isSelected = false });
        this.toggle = function (member) {
            member.isSelected = !member.isSelected;
        }
        this.save = function () {
            var members = self.members.filter(function (m) {
                return m.isSelected;
            })
            var project = {};
            project.title = self.title;
            project.tags = [];
            project.tasks = [];
            project.isNew = true;
            project.members = members;
            Todo.projects.push(project);
            ActionsMenu.close();
        }
    })