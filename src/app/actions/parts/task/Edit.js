angular
    .module('app')
    .directive('taskEdit', function () {
        return {
            restrict: 'E',
            templateUrl: 'app/actions/parts/task/Edit.html',
            controller: "TaskEdit",
            controllerAs: "taskEdit"
        }
    }).controller('TaskEdit', function (ActionsMenu, Todo) {
        var self = this;
        this.taskCopy = angular.copy(Todo.selectedTask);
        this.todo = Todo;
        this.taskCopy.tags = this.taskCopy.tags.join(', ');
        this.members = angular.copy(Todo.selectedProject.members);
        this.taskCopy.members.forEach(function (m) {
            self.members.forEach(function(minner){
                if(m.id == minner.id) {
                    minner.isSelected = true;
                }
            })
        })
        this.toggle = function (member) {
            member.isSelected = !member.isSelected;
        }
        this.save = function () {
            var members = self.members.filter(function (m) {
                return m.isSelected;
            })
            Todo.selectedTask = self.taskCopy;
            ActionsMenu.close();
        }
    })