angular
    .module('app')
    .directive('taskCreate', function () {
        return {
            restrict: 'E',
            templateUrl: 'app/actions/parts/task/Create.html',
            controller: "TaskCreate",
            controllerAs: "taskCreate"
        }
    }).controller('TaskCreate', function (ActionsMenu, Todo) {
        var self = this;
        this.todo = Todo;
        this.task = {
            date: moment(new Date()),
            priority: "white" 
        };
        this.members = angular.copy(Todo.selectedProject.members);
        this.members.forEach(function (m) {
            m.isSelected = false;
        })
        this.toggle = function (member) {
            member.isSelected = !member.isSelected;
        }
        this.save = function () {
            var members = self.members.filter(function (m) {
                return m.isSelected;
            })
            this.task.members = members;
            var task = angular.copy(this.task);
            var date = this.task.date.format("YYYY.MM.DD");
            var isPushed = false;
            task.tags = task.tags.split(',').map(function (tag) {
                return tag.trim();
            })
            task.creator = Todo.user;
            task.isNew = true;
            task.date = date;
            for (i in Todo.selectedProject.tasks) {
                if (Todo.selectedProject.tasks[i].date == date) {
                    Todo.selectedProject.tasks[i].tasks.push(task);
                    isPushed = true;
                }
            }
            if (!isPushed) {
                Todo.selectedProject.tasks.unshift({
                    'date': date,
                    'tasks': [task]
                })
            }
            Todo.selectedProject.count++;
            ActionsMenu.close();
        }
    })