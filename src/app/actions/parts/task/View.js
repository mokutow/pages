angular
    .module('app')
    .directive('taskView', function () {
        return {
            restrict: 'E',
            templateUrl: 'app/actions/parts/task/View.html',
            controller: "TaskView",
            controllerAs: "taskView"
        }
    }).controller('TaskView', function (ActionsMenu, Todo) {
        var self = this;
        this.deleteTask = function () {
            var day = Todo.selectedDay;
            var task = Todo.selectedTask;
            day.tasks.splice(day.tasks.indexOf(task), 1);
            Todo.selectedProject.count--;
            if (day.tasks.length == 0) {
                Todo.selectedProject.tasks.splice(Todo.selectedProject.tasks.indexOf(day), 1);
            }
            ActionsMenu.close();
        }
        this.task = angular.copy(Todo.selectedTask);
        this.date = moment(this.task.date.split('.'));
        this.getTagColor = function (tag) {
            for (var i = 0; i < Todo.selectedProject.tags.length; i++) {
                if (Todo.selectedProject.tags[i].title == tag) {
                    return Todo.selectedProject.tags[i].color;
                }
            }
            return "blue";
        }
        this.editTask = ActionsMenu.Task.edit;
        this.toggle = function (member) {
            member.isSelected = !member.isSelected;
        }
    })