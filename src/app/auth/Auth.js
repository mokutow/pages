angular
  .module('app')
  .directive('authForgot', function () {
    return {
      restrict: 'E',
      templateUrl: 'app/auth/Forgot.html'
    }
  })
  .directive('authLogin', function () {
    return {
      restrict: 'E',
      templateUrl: 'app/auth/Login.html',
      controller: 'LoginCtrl',
      controllerAs: 'login'
    }
  })
  .directive('authSignup', function () {
    return {
      restrict: 'E',
      templateUrl: 'app/auth/Signup.html',
      controller: 'SignupCtrl',
      controllerAs: 'signup'
    }
  })
  .factory('AuthService', function ($location) {
    var auth = {};
    auth.login = function () {
      $location.path('/app');
    }
    auth.signup = function () {
      $location.path('/app');
    }
    auth.logout = function () {
      $location.path('/login');
    }
    return auth;
  })
  .controller('LoginCtrl', function (AuthService) {
    this.login = AuthService.login;
  })
  .controller('SignupCtrl', function (AuthService, $location) {
    this.signup = AuthService.signup;
    if ($location.search().inviteId) {
      this.invite = {
        title: 'You were invited, Honey'
      }
      this.email = 'honey@honey.com'
    }
  })
